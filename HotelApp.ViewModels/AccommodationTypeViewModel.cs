﻿namespace HotelApp.ViewModels
{
    public class AccommodationTypeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
