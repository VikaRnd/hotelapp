﻿namespace HotelApp.ViewModels
{
    public class RoomViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Capacity { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }

        public int AccommodationId { get; set; }
        public AccommodationViewModel Accommodation { get; set; }
    }
}
