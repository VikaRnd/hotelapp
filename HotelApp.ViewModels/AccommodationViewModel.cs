﻿using System.Collections.Generic;

namespace HotelApp.ViewModels
{
    public class AccommodationViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int AccommodationTypeId { get; set; }
        public AccommodationTypeViewModel AccommodationType { get; set; }
        public ICollection<RoomViewModel> Rooms { get; set; }
    }
}
