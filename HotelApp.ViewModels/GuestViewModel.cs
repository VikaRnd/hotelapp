﻿using System.Collections.Generic;

namespace HotelApp.ViewModels
{
    public class GuestViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<BookingViewModel> Bookings { get; set; }
    }
}
