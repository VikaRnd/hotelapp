﻿using System;

namespace HotelApp.ViewModels
{
    public class BookingViewModel
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal Total { get; set; }
        public bool IsNew { get; set; }

        public int GuestId { get; set; }
        public GuestViewModel Guest { get; set; }

        public int AccommodationId { get; set; }
        public AccommodationTypeViewModel AccommodationType { get; set; }
    }
}
