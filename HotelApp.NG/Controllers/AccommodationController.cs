using System;
using Microsoft.AspNetCore.Mvc;
using HotelApp.BL.Contracts;
using Microsoft.Extensions.Logging;
using HotelApp.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace HotelApp.NG.Controllers
{
  [Authorize]
  [Route("api/accommodations")]
  public class AccommodationController : Controller
  {
        IAccommodationService _accommodationService;
        ILogger<AccommodationController> _logger;

        public AccommodationController(IAccommodationService accommodationService, ILogger<AccommodationController> logger)
        {
          _accommodationService = accommodationService;
          _logger = logger;
        }

        [HttpGet]
        public IActionResult Get()
        {
          try
          {
            var accommodations = _accommodationService.GetAll();
            if (accommodations == null)
            {
              _logger.LogError($"Accommodations have not been found.");
              return NotFound();
            }

            return Ok(accommodations);
          }
          catch (Exception ex)
          {
            _logger.LogError(ex.Message);
            return StatusCode(500, ex.Message);
          }
        }

        [HttpGet]
        [Route("{id:int}", Name = "GetSingleAccommodation")]
        public IActionResult Get(int id)
        {
          try
          {
            var accommodation = _accommodationService.Get(a => a.Id == id);
            if (accommodation == null)
            {
              _logger.LogError($"Accommodation with id {id} does not exists.");
            }
            return Ok(accommodation);
          }
          catch (Exception ex)
          {
            _logger.LogError(ex.Message);
            return StatusCode(500, ex.Message);
          }

        }

        [Authorize]
        [HttpPost]
        public IActionResult Create(AccommodationViewModel model)
        {
          if (model == null)
          {
            return BadRequest();
          }

          if (!ModelState.IsValid)
          {
            return BadRequest(ModelState);
          }

          try
          {
            var newModelID = _accommodationService.Insert(model);
            return CreatedAtRoute("GetSingleAccommodation", new { id = newModelID }, model);
          }
          catch (Exception ex)
          {
            _logger.LogError(ex.Message);
            return StatusCode(500, ex.Message);
          }
        }

        [HttpPut("{id:int}")]
        public IActionResult Update(int id, AccommodationViewModel model)
        {
          if (model == null)
          {
            return BadRequest();
          }

          if (!ModelState.IsValid)
          {
            return BadRequest(ModelState);
          }

          try
          {
            _accommodationService.Update(model);
          }
          catch (Exception ex)
          {
            _logger.LogError(ex.Message);
            return StatusCode(500, ex.Message);
          }

          return NoContent();
        }

        [HttpDelete("{id:int}")]
        public IActionResult Delete(int id)
        {
          try
          {
            _accommodationService.Delete(id);
          }
          catch (Exception ex)
          {
            _logger.LogError(ex.Message);
            return StatusCode(500, ex.Message);
          }

          return NoContent();
        }
  }
}
