using HotelApp.BL.Contracts;
using HotelApp.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HotelApp.NG.Controllers
{
  public class AccountController : Controller
  {
    IUserService _userService;
    ILogger<AccommodationController> _logger;

    public AccountController(IUserService userService, ILogger<AccommodationController> logger)
    {
      _userService = userService;
      _logger = logger;
    }

    [HttpGet]
    public IActionResult Login()
    {
      return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Login(UserViewModel model)
    {
      if (ModelState.IsValid)
      {
        var user = _userService.Get(u => u.Email == model.Email);
        if (user != null)
        {
          if (user.Password == model.Password)
          {
            await Authenticate(model.Email);

            if(model.Role.Name == "Operator")
              return RedirectToAction("Index", "OperatorPage");
            else
              ModelState.AddModelError("incorrectRole", "Incorrect User Role");
          }
          else
            ModelState.AddModelError("incorrectCredentials", "Incorrect credentials!");
        }
        else
          return View("SuggestToRegister");
      }

      return View(model);
    }

    [HttpGet]
    public IActionResult Register()
    {
      return View("Register");
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Register(RegisterViewModel model)
    {
      if (ModelState.IsValid)
      {
        var user = _userService.Get(u => u.Email == model.Email);
        if (user == null)
        {
          _userService.Insert(new UserViewModel { Email = model.Email, Password = model.Password });
          await Authenticate(model.Email);
          return RedirectToAction("Get", "Accommodation");
        }
        else
        {
          if (model.Email == user.Email)
            ModelState.AddModelError("Email", "This login is occupied. Thy another one");
        }
      }
      else
      {
        ModelState.AddModelError("incorrectCredentials", "Incorrect credentials!");
        //return BadRequest(ModelState);
      }
      return View(model);
    }

    public async Task<IActionResult> Logout()
    {
      await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
      return RedirectToAction("Login", "Account");
    }

    //Объекты claim представляют некоторую информацию о пользователе, которую мы можем использовать для авторизации в приложении.
    // у пользователя могут быть некоторые признаки, которые представляют отдельный объект claim
    // в зависимости от значения этих claim мы можем предоставлять пользователю доступ к тому или иному ресурсу
    // Claims представляют более общий механизм авторизации нежели стандартные логины или роли, которые привязаны лишь к одному определенному признаку пользователя
    private async Task Authenticate(string userName)
    {
      var claims = new List<Claim>
          {
              new Claim(ClaimsIdentity.DefaultNameClaimType, userName)    // DefaultNameClaimType - тип - логин, userName - поле - значение логина
          };

      // С помощью объекта ClaimsIdentity мы можем управлять объектами claim у текущего пользователя.
      // сlaims - ассоциированные с пользователем объекты claim (будут лежать в HttpContext.User.Claims)
      ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

      // ClaimsPrincipal - это объект держатель Claim'ов, т.е. HttpContext.User, который позволяет работать с claims
      await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
    }

  }
}
