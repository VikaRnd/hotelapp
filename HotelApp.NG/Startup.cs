using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using HotelApp.DAL.Repositories;
using HotelApp.BL.Contracts;
using HotelApp.BL.Services;
using Microsoft.Extensions.Configuration;
using HotelApp.DAL;
using HotelApp.ViewModels;
using HotelApp.DAL.Models;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using HotelApp.DAL.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace HotelApp.NG
{
  public class Startup
  {
    public static IConfiguration Configuration;

    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddTransient<IAccommodationRepository, AccommodationRepository>();
      services.AddTransient<IAccommodationTypeRepository, AccommodationTypeRepository>();
      services.AddTransient<IBookingRepository, BookingRepository>();
      services.AddTransient<IGuestRepository, GuestRepository>();
      services.AddTransient<IRoomRepository, RoomRepository>();
      services.AddTransient<IUserRepository, UserRepository>();

      services.AddTransient<IAccommodationService, AccommodationService>();
      services.AddTransient<IAccommodationTypeService, AccommodationTypeService>();
      services.AddTransient<IBookingService, BookingService>();
      services.AddTransient<IGuestService, GuestService>();
      services.AddTransient<IRoomService, RoomService>();
      services.AddTransient<IUserService, UserService>();

      var connectionString = Startup.Configuration["connectionStrings:hotelDBConnection"];
      services.AddDbContext<HotelAppEFContext>(o => o.UseSqlServer(connectionString).EnableSensitiveDataLogging());

      services.AddIdentity<User, IdentityRole<int>>()
          //.AddEntityFrameworkStores<HotelAppEFContext>()
          .AddDefaultTokenProviders();

      services.AddAutoMapper();
      services.AddAuthentication(options =>
      {
        options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
        options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
      }).AddCookie(options =>
              {
                  options.LoginPath = new PathString("/Account/Login");
                  options.LogoutPath = new PathString("/Account/Logout");
                  options.ExpireTimeSpan = TimeSpan.FromHours(1);
                  options.SlidingExpiration = true;
              });

      services.AddMvc();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env, HotelAppEFContext context, ILoggerFactory loggerFactory)
    {
      app.UseDefaultFiles();
      app.UseStaticFiles();

      loggerFactory.AddDebug();
      loggerFactory.AddNLog();

      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      context.EnsureSeedDataForContext();

      app.UseAuthentication();

      app.UseMvc(
          routes =>
          {
            routes.MapRoute("autorize", "{controller=Account}/{action=Login}");
            routes.MapRoute("register", "{controller=Account}/{action=Register}");
            routes.MapRoute("Index", "{controller=Home}/{action=OperatorPage}");
          }
        );
      
    }
  }
}
