﻿using HotelApp.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace HotelApp.DAL.Repositories
{
    public abstract class Repository<TEntity, TContext> : IRepository<TEntity> 
            where TEntity : class
            where TContext : DbContext
    {
        private TContext _context;
        protected abstract DbSet<TEntity> DbSet { get; }

        public Repository(TContext context)
        {
            _context = context;
        }

        public virtual IList<TEntity> GetAll()
        {
            return DbSet.ToList();
        }

        public virtual TEntity Get(Expression<Func<TEntity, bool>> filter = null)
        {
            return DbSet.AsNoTracking().FirstOrDefault(filter);
        }

        public virtual int Insert(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Added;
            _context.SaveChanges();
            return 0;
        }

        public void Update(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            TEntity entity = DbSet.Find(id);
            if (_context.Entry(entity).State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }
            _context.Entry(entity).State = EntityState.Deleted;
            _context.SaveChanges();
        }
    }
}
