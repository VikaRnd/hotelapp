﻿using HotelApp.DAL.Contracts;
using HotelApp.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace HotelApp.DAL.Repositories
{
    public class UserRepository : Repository<User, HotelAppEFContext>, IUserRepository
    {
        private HotelAppEFContext _context;
        public UserRepository(HotelAppEFContext context) : base(context)
        {
            _context = context;
        }

        protected override DbSet<User> DbSet
        {
            get { return _context.Users; }
        }
        
        public override int Insert(User entity)
        {
            this.DbSet.Add(entity);
            _context.SaveChanges();
            return entity.Id;
        }
    }
}
