﻿using HotelApp.DAL.Contracts;
using HotelApp.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotelApp.DAL.Repositories
{
    public class RoomRepository : Repository<Room, HotelAppEFContext>, IRoomRepository
    {
        private HotelAppEFContext _context;
        public RoomRepository(HotelAppEFContext context) : base(context)
        {
            _context = context;
        }

        protected override DbSet<Room> DbSet
        {
            get { return _context.Rooms; }
        }

        public override int Insert(Room entity)
        {
            this.DbSet.Add(entity);
            _context.SaveChanges();
            return entity.Id;
        }
    }
}
