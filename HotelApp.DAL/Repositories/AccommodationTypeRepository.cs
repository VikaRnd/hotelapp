﻿using HotelApp.DAL.Contracts;
using HotelApp.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotelApp.DAL.Repositories
{
    public class AccommodationTypeRepository :  Repository<AccommodationType, HotelAppEFContext>, IAccommodationTypeRepository
    {
        private HotelAppEFContext _context;
        public AccommodationTypeRepository(HotelAppEFContext context) : base(context)
        {
            _context = context;
        }

        protected override DbSet<AccommodationType> DbSet
        {
            get { return _context.AccommodationTypes; }
        }

        public override int Insert(AccommodationType entity)
        {
            this.DbSet.Add(entity);
            _context.SaveChanges();
            return entity.Id;
        }
    }
}
