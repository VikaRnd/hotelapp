﻿using HotelApp.DAL.Contracts;
using HotelApp.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotelApp.DAL.Repositories
{
    public class GuestRepository : Repository<Guest, HotelAppEFContext>, IGuestRepository
    {
        private HotelAppEFContext _context;
        public GuestRepository(HotelAppEFContext context) : base(context)
        {
            _context = context;
        }

        protected override DbSet<Guest> DbSet
        {
            get { return _context.Guests; }
        }

        public override int Insert(Guest entity)
        {
            this.DbSet.Add(entity);
            _context.SaveChanges();
            return entity.Id;
        }
    }
}
