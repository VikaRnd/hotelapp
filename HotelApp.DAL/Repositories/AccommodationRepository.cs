﻿using HotelApp.DAL.Contracts;
using HotelApp.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace HotelApp.DAL.Repositories
{
    public class AccommodationRepository : Repository<Accommodation, HotelAppEFContext>, IAccommodationRepository
    {
        private HotelAppEFContext _context;
        public AccommodationRepository(HotelAppEFContext context) : base(context)
        {
            _context = context;
        }

        protected override DbSet<Accommodation> DbSet
        {
            get { return _context.Accommodations; }
        }

        public override IList<Accommodation> GetAll()
        {
            return DbSet
                .AsNoTracking()
                .Include(p=>p.AccommodationType)
                .Include(p=>p.Rooms)
                .ToList();
        }

        public override Accommodation Get(Expression<Func<Accommodation, bool>> filter = null)
        {
            return DbSet
                .AsNoTracking()
                .Include(p=>p.AccommodationType)
                .Include(p=>p.Rooms)
                .FirstOrDefault(filter);
        }
        
        public override int Insert(Accommodation entity)
        {
            this.DbSet.Add(entity);
            _context.SaveChanges();
            return entity.Id;
        }
    }
}
