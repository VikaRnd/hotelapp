﻿using HotelApp.DAL.Contracts;
using HotelApp.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotelApp.DAL.Repositories
{
    public class BookingRepository : Repository<Booking, HotelAppEFContext>, IBookingRepository
    {
        private HotelAppEFContext _context;
        public BookingRepository(HotelAppEFContext context) : base(context)
        {
            _context = context;
        }

        protected override DbSet<Booking> DbSet
        {
            get { return _context.Bookings; }
        }

        public override int Insert(Booking entity)
        {
            this.DbSet.Add(entity);
            _context.SaveChanges();
            return entity.Id;
        }
    }
}
