﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelApp.DAL.Models
{
    public class Room
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]

        [Range(0, 20, ErrorMessage = "Capacity must be in a range from 1 to 20")]
        public int Capacity { get; set; }

        [Range(0.01, 999999999, ErrorMessage = "Price must be greater than 0.00")]
        public decimal Price { get; set; }

        [Required]
        [MaxLength(200)]
        public string Description { get; set; }

        [ForeignKey("AccommodationId")]
        public Accommodation Accommodation { get; set; }
        public int AccommodationId { get; set; }
    }
}
