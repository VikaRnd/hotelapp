﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelApp.DAL.Models
{
    public class Accommodation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [ForeignKey("AccommodationTypeId")]
        public AccommodationType AccommodationType { get; set; }
        public int AccommodationTypeId { get; set; }

        public ICollection<Room> Rooms { get; set; } = new List<Room>();
    }
}
