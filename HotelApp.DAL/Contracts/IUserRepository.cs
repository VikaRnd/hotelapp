﻿using HotelApp.DAL.Models;
using HotelApp.DAL.Repositories;

namespace HotelApp.DAL.Contracts
{
    public interface IUserRepository : IRepository<User>
    {
    }
}
