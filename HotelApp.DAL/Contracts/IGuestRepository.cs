﻿using HotelApp.DAL.Models;
using HotelApp.DAL.Repositories;

namespace HotelApp.DAL.Contracts
{
    public interface IGuestRepository : IRepository<Guest>
    {
    }
}
