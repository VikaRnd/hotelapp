﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace HotelApp.DAL.Repositories
{
    public interface IRepository<TEntity> 
        where TEntity : class
    {
        IList<TEntity> GetAll();
        TEntity Get(Expression<Func<TEntity, bool>> filter = null);
        int Insert(TEntity entity);
        void Update(TEntity entity);
        void Delete(int id);
    }
}
