﻿using Microsoft.EntityFrameworkCore;
using HotelApp.DAL.Models;

namespace HotelApp.DAL
{
    public class HotelAppEFContext : DbContext
    {
        public HotelAppEFContext(DbContextOptions<HotelAppEFContext> options)
            : base(options)
        {
            Database.EnsureCreated();
            //Database.Migrate();
        }

        public DbSet<AccommodationType> AccommodationTypes { get; set; }
        public DbSet<Accommodation> Accommodations { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Guest> Guests { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
    }
}
