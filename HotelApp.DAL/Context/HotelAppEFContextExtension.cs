﻿using HotelApp.DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace HotelApp.DAL
{
    public static class HotelAppEFContextExtension
    {
        public static void EnsureSeedDataForContext(this HotelAppEFContext context)
        {
            SeedAccommodationTypes(context);
            SeedAccommodations(context);
            SeedRooms(context);
            SeedGuests(context);
            SeedRoles(context);
            SeedUsers(context);

            context.SaveChanges();
        }

        private static void SeedAccommodationTypes(HotelAppEFContext context)
        {
            if (context.AccommodationTypes.Any())
                return;

            var accTypes = new List<AccommodationType>()
            {
                new AccommodationType()
                {
                    Name = "Private Apartment"
                },
                new AccommodationType()
                {
                    Name = "Hotel"
                },
                new AccommodationType()
                {
                    Name = "Hostel"
                }
            };

            context.AccommodationTypes.AddRange(accTypes);
        }

        private static void SeedAccommodations(HotelAppEFContext context)
        {
            if (context.Accommodations.Any())
                return;

            var accs = new List<Accommodation>()
            {
                new Accommodation
                {
                    Name = "Bristol Hotel",
                    AccommodationTypeId = 2
                }
            };

            context.Accommodations.AddRange(accs);
        }

        private static void SeedRooms(HotelAppEFContext context)
        {
            if (context.Rooms.Any())
                return;

            var rooms = new List<Room>()
            {
                new Room()
                {
                    Name = "Single Room",
                    Capacity = 1,
                    AccommodationId = 1,
                    Price = 50.00M,
                    Description = "Classic single rooms (19 m²) are tastefully decorated for guests looking for classic elegance and comfort."
                }
            };

            context.Rooms.AddRange(rooms);
            context.SaveChanges();
        }

        private static void SeedGuests(HotelAppEFContext context)
        {
            if (context.Guests.Any())
                return;

            var guests = new List<Guest>()
            {
                new Guest()
                {
                    Name = "Scott Cornel"
                }
            };

            context.Guests.AddRange(guests);
        }

        private static void SeedRoles(HotelAppEFContext context)
        {
            if (context.Roles.Any())
                return;

            var roles = new List<Role>()
            {
                new Role { Name = "Operator" },
                new Role { Name = "Owner" },
                new Role { Name = "Guest" }
            };

            context.Roles.AddRange(roles);
        }

        private static void SeedUsers(HotelAppEFContext context)
        {
            if (context.Users.Any())
                return;

            var users = new List<User>()
            {
                new User()
                {
                    Email = "ScottCornel@gmail.com",
                    Password = "qwerty",
                    RoleId = 1
                },
                new User()
                {
                    Email = "AnnaIvanova@gmail.com",
                    Password = "qwerty",
                    RoleId = 2
                },
                new User()
                {
                    Email = "RobertMokras@gmail.com",
                    Password = "qwerty",
                    RoleId = 3
                }
            };

            context.Users.AddRange(users);
        }
    }
}
