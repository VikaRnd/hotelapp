﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace HotelApp.DAL.Context
{
    public class ContextFactory : IDesignTimeDbContextFactory<HotelAppEFContext>
    {
        public HotelAppEFContext CreateDbContext(string[] args)
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var optionsBuilder = new DbContextOptionsBuilder<HotelAppEFContext>();

            var connectionString = configuration.GetConnectionString("hotelDBConnection");

            optionsBuilder.UseSqlServer(connectionString);

            return new HotelAppEFContext(optionsBuilder.Options);
        }
    }
}
