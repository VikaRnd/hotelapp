﻿using AutoMapper;
using HotelApp.DAL.Models;
using HotelApp.ViewModels;
using System.Collections.Generic;

namespace HotelApp.BL.Mappers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Accommodation, AccommodationViewModel>();
            CreateMap<AccommodationViewModel, Accommodation>();

            CreateMap<AccommodationTypeViewModel, AccommodationType>();
            CreateMap<AccommodationType, AccommodationTypeViewModel>();

            CreateMap<BookingViewModel, Booking>();
            CreateMap<Booking, BookingViewModel>();

            CreateMap<GuestViewModel, Guest>();
            CreateMap<Guest, GuestViewModel>();

            CreateMap<RoomViewModel, Room>();
            CreateMap<Room, RoomViewModel>();

            CreateMap<List<Room>, List<RoomViewModel>>();
            CreateMap<List<RoomViewModel>, List<Room>>();

            CreateMap<UserViewModel, User>();
            CreateMap<User, UserViewModel>();

            CreateMap<RoleViewModel, Role>();
            CreateMap<Role, RoleViewModel>();
        }
    }
}
