﻿using AutoMapper;
using System.Collections.Generic;

namespace HotelApp.BL.Mappers
{
    public static class AutoMapperExtension
    {
        public static IList<TDestination> MapList<TSource, TDestination>(this IMapper mapper, IList<TSource> source)
        {
            return mapper.Map<List<TDestination>>(source);
        }
    }
}
