﻿using HotelApp.BL.Contracts;
using HotelApp.DAL.Models;
using AutoMapper;
using HotelApp.ViewModels;
using HotelApp.DAL.Repositories;
using HotelApp.DAL.Contracts;

namespace HotelApp.BL.Services
{
    public class AccommodationService : GenericService<AccommodationViewModel, Accommodation, AccommodationRepository>, IAccommodationService
    {
        public AccommodationService(IAccommodationRepository repository, IMapper mapper) 
            : base(repository, mapper)
        {
        }
    }
}
