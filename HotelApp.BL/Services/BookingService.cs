﻿using HotelApp.BL.Contracts;
using HotelApp.DAL.Models;
using HotelApp.DAL.Contracts;
using AutoMapper;
using HotelApp.ViewModels;
using HotelApp.DAL.Repositories;

namespace HotelApp.BL.Services
{
    public class BookingService : GenericService<BookingViewModel, Booking, BookingRepository>, IBookingService
    {
        public BookingService(IBookingRepository repository, IMapper mapper)
            : base(repository, mapper)
        {
        }
    }
}
