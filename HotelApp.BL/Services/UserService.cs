﻿using HotelApp.DAL.Models;
using AutoMapper;
using HotelApp.ViewModels;
using HotelApp.DAL.Repositories;
using HotelApp.DAL.Contracts;
using HotelApp.BL.Contracts;

namespace HotelApp.BL.Services
{
    public class UserService : GenericService<UserViewModel, User, UserRepository>, IUserService
    {
        public UserService(IUserRepository repository, IMapper mapper) 
            : base(repository, mapper)
        {
        }
    }
}
