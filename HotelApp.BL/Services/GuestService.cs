﻿using HotelApp.BL.Contracts;
using HotelApp.DAL.Models;
using AutoMapper;
using HotelApp.ViewModels;
using HotelApp.DAL.Repositories;
using HotelApp.DAL.Contracts;

namespace HotelApp.BL.Services
{
    public class GuestService : GenericService<GuestViewModel, Guest, GuestRepository>, IGuestService
    {
        public GuestService(IGuestRepository repository, IMapper mapper)
            : base(repository, mapper)
        {
        }
    }
}
