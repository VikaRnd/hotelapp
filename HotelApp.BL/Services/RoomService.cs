﻿using HotelApp.BL.Contracts;
using HotelApp.DAL.Models;
using AutoMapper;
using HotelApp.ViewModels;
using HotelApp.DAL.Repositories;
using HotelApp.DAL.Contracts;

namespace HotelApp.BL.Services
{
    public class RoomService : GenericService<RoomViewModel, Room, RoomRepository>, IRoomService
    {
        public RoomService(IRoomRepository repository, IMapper mapper) 
            : base(repository, mapper)
        {
        }
    }
}
