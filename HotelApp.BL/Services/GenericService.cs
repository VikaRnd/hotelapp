﻿using AutoMapper;
using HotelApp.BL.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq.Expressions;
using Abp.Domain.Entities;
using HotelApp.DAL.Models;
using Microsoft.EntityFrameworkCore;
using HotelApp.DAL.Repositories;
using HotelApp.BL.Mappers;

namespace HotelApp.BL.Services
{
    public class GenericService<TEntityViewModel, TEntity, TRepository> : IService<TEntityViewModel, TEntity, TRepository>
        where TRepository : IRepository<TEntity>
        where TEntity : class
    {
        TRepository _repository;
        readonly IMapper _mapper;

        public GenericService(IRepository<TEntity> repository, IMapper mapper)
        {
            _repository = (TRepository)repository;
            _mapper = mapper;
        }

        public IList<TEntityViewModel> GetAll()
        {
            var entities = _repository.GetAll();
            var entitiesVM = _mapper.MapList<TEntity, TEntityViewModel>(entities);
            return entitiesVM;
        }

        public TEntityViewModel Get(Expression<Func<TEntityViewModel, bool>> filter = null)
        {
            var mappedFilter = _mapper.Map<Expression<Func<TEntity, bool>>>(filter);
            var entity = _repository.Get(mappedFilter);
            var entityVM = _mapper.Map<TEntityViewModel>(entity);

            return entityVM;
        }

        public int Insert(TEntityViewModel entityVM)
        {
            var entity = _mapper.Map<TEntity>(entityVM);
            int newEntityId = _repository.Insert(entity);
            return newEntityId;
        }

        public void Update(TEntityViewModel entityVM)
        {
            var entity = _mapper.Map<TEntity>(entityVM);
            _repository.Update(entity);
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }
    }
}
