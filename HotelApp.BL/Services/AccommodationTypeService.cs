﻿using HotelApp.BL.Contracts;
using HotelApp.DAL.Models;
using HotelApp.ViewModels;
using HotelApp.DAL.Repositories;
using AutoMapper;
using HotelApp.DAL.Contracts;

namespace HotelApp.BL.Services
{
    public class AccommodationTypeService : GenericService<AccommodationTypeViewModel, AccommodationType, AccommodationTypeRepository>, IAccommodationTypeService
    {
        public AccommodationTypeService(IAccommodationTypeRepository repository, IMapper mapper) 
            : base(repository, mapper)
        {
        }
    }
}
