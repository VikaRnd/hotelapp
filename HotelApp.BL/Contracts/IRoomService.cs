﻿using HotelApp.DAL.Models;
using HotelApp.DAL.Repositories;
using HotelApp.ViewModels;

namespace HotelApp.BL.Contracts
{
    public interface IRoomService : IService<RoomViewModel, Room, IRepository<Room>>
    {

    }
}
