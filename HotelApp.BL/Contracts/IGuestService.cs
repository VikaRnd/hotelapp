﻿using HotelApp.DAL.Models;
using HotelApp.DAL.Repositories;
using HotelApp.ViewModels;

namespace HotelApp.BL.Contracts
{
    public interface IGuestService : IService<GuestViewModel, Guest, IRepository<Guest>>
    {
    }
}
