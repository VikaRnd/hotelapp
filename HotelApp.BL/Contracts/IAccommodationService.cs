﻿using HotelApp.DAL.Models;
using HotelApp.DAL.Repositories;
using HotelApp.ViewModels;

namespace HotelApp.BL.Contracts
{
    public interface IAccommodationService : IService<AccommodationViewModel, Accommodation, IRepository<Accommodation>>
    {
    }
}
