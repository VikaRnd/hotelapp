﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace HotelApp.BL.Contracts
{
    public interface IService<TEntityViewModel, TEntity, TRepository>
    {
        IList<TEntityViewModel> GetAll();
        TEntityViewModel Get(Expression<Func<TEntityViewModel, bool>> filter = null);
        int Insert(TEntityViewModel entityVM);
        void Update(TEntityViewModel entityVM);
        void Delete(int id);
    }
}
