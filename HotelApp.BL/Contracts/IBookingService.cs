﻿using HotelApp.DAL.Models;
using HotelApp.DAL.Repositories;
using HotelApp.ViewModels;

namespace HotelApp.BL.Contracts
{
    public interface IBookingService : IService<BookingViewModel, Booking, IRepository<Booking>>
    {
    }
}
